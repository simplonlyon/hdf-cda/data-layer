using Microsoft.AspNetCore.Mvc;
using Simplon.Data.Entity;
using Simplon.Data.Repository;

namespace Simplon.Data.Controller;

[Route("/api/dog")] 
[ApiController]
public class DogController:ControllerBase {
    private DogRepository repo = new DogRepository();

    [HttpGet]
    public ActionResult<List<Dog>> GetAll([FromQuery]string? search = null, [FromQuery] int page = 1, [FromQuery] int pageSize = 10) {
        Console.WriteLine(search);
        if(search != null)  {
            return repo.Search(search);
        }
        return repo.FindAll(pageSize, (page-1)*pageSize);
    }
    
    [HttpGet("{id}")] //http://localhost:8080/api/dog/10 GET
    public ActionResult<Dog> GetOne(int id) {
        var dog = repo.Find(id);
        if(dog == null) {
            return NotFound();
        }
        return dog;
    }

    [HttpPost] //http://localhost:8080/api/dog POST
    public IActionResult add(Dog dog) {
        repo.Save(dog);
        //Pas absolument obligatoire de renvoyer ça plutôt que directement le chien mais c'est pour faire que le status http soit bien 201 Created plutôt que 200 Ok
        return Created($"/api/dog/{dog.Id}", dog);
    }

    /*
    //Version alternative sans IActionResult mais avec un attribute
    [HttpPost]
    [ProducesResponseType(StatusCodes.Status201Created)]
    public Dog add(Dog dog) {
        repo.Save(dog);
        return dog;
    
    }
    */

    [HttpDelete("{id}")]
    public ActionResult<Dog> DeleteOne(int id) {
        var dog = repo.Find(id);
        if(dog == null) {
            return NotFound();
        }
        repo.Delete(dog);
        return NoContent();
    }

    [HttpPut("{id}")]
    public ActionResult<Dog> UpdateOne(int id, Dog dog) {
        var toUpdate = repo.Find(id);
        if(toUpdate == null) {
            return NotFound();
        }
        dog.Id = id;
        repo.Update(dog);
        // toUpdate.Name = dog.Name;
        // toUpdate.Breed = dog.Breed;
        // toUpdate.BirthDate = dog.BirthDate;
        // repo.Update(toUpdate);
        return dog;
    }

    //Le Patch est fait pour faire des mise à jour partielle, ceci dit actuellement il ne passera pas si on
    //ne lui fournit pas de name dans la requête car on a rajouté une validation Required dans l'entité. Une solution
    //pour contourner ce soucis serait de créer une entité dédiée à la mise à jour PatchDog par exemple (c'est ce
    //qu'on appel un DTO) qui elle n'aurait pas le required sur le name
    [HttpPatch("{id}")]
    public ActionResult<Dog> PatchOne(int id, Dog dog) {
        var toUpdate = repo.Find(id);
        if(toUpdate == null) {
            return NotFound();
        }
        if(dog.Name != null)  {
            toUpdate.Name = dog.Name;

        }
        if(dog.Breed != null) {
            toUpdate.Breed = dog.Breed;

        }
        if(dog.BirthDate != null) {
            toUpdate.BirthDate = dog.BirthDate;

        }
        repo.Update(toUpdate);
        return toUpdate;
    }

    
    [HttpGet("breed")]
    public ActionResult<Dictionary<string,int>> BreedsCount() {
        
        return repo.CountByBreed();
    }

}