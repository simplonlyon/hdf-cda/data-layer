
using Microsoft.AspNetCore.Mvc;
using Simplon.Data.Entity;

namespace Simplon.Data.Controller;

[ApiController]
[Route("/api/first")]
public class FirstController: ControllerBase {

    [HttpGet]
    public string example() {
        return "bonjour";
    }

    [HttpPost("machin")]
    public Dog postExample(Dog dog) {

        return dog;
    }
}