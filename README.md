# Data Layer
Projet d'exercices sur les composants d'accès aux données en C#

Egalement une API Rest faite avec le framework .NET (version faite à la main avec les HttpListener consultable sur [ce tag](https://gitlab.com/simplonlyon/hdf-cda/data-layer/-/tree/httplistener?ref_type=tags))

## How To Use
1. Créer une base de données (par exemple : `CREATE DATABASE my_database`)
2. Importer le schéma dans celle ci (`mysql -u simplon -p my_database < db.sql`, ou via workbench ou une extension peu importe)
3. Modifier les informations de connexion à la base de données

